import { BrowserWindow, Menu, shell } from 'electron';
import * as path from 'path';
import { default as url } from 'url';

Menu.setApplicationMenu(
    Menu.buildFromTemplate([
        {
            submenu: [
                {
                    click: (_, browserWindow) => {
                        browserWindow.webContents.openDevTools();
                    },
                    label: 'Open Debugger'
                },
                {
                    label: 'Copy',
                    accelerator: 'CmdOrCtrl+C',
                    role: 'copy'
                },
                {
                    click: (_, browserWindow) => {
                        browserWindow.webContents.reload();
                    },
                    label: 'Refresh'
                },
                {
                    click: (_, browserWindow) => {
                        browserWindow.minimize();
                    },
                    label: 'Minimize'
                }
            ],
            label: 'File'
        }
    ])
);

export class StatusWindowManager {
    private static _instance: StatusWindowManager;

    private win: BrowserWindow;

    private constructor() {
        this.win = new BrowserWindow({
            webPreferences: {
                nodeIntegration: true
            },
            title: 'Our Main Window'
        });
        this.win.maximize();
        this.win.webContents.openDevTools();

        this.win.loadURL('http://127.0.0.1:8082/src/gui/scenes/status.html');

        // new links will open in a new Electron window, without an exit button
        // this redirects all pages to a browser window.
        this.win.webContents.on('new-window', (e, url) => {
            e.preventDefault();
            shell.openExternal(url);
        });
    }

    static getInstance() {
        if (!StatusWindowManager._instance) {
            StatusWindowManager._instance = new StatusWindowManager();
        }
        return StatusWindowManager._instance;
    }
}
